import React, { useState } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { withStyles } from "@material-ui/core";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import IconButton from "@material-ui/core/IconButton";
import KeyboardBackspace from "@material-ui/icons/KeyboardBackspace";
import { withRouter } from "react-router-dom";
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";
import ponyBackground from "pony.jpg";

const theme = createMuiTheme({
  palette: {
    type: "dark",
    primary: {
      main: "#FFF",
      contrastText: "#FFF"
    },
    secondary: {
      main: "#79070c",
      // dark: will be calculated from palette.secondary.main,
      contrastText: "#FFF"
    }
    // error: will use the default color
  },
  overrides: {
    MuiButton: {
      text: {
        background: "#FE6B8B",
        borderRadius: 3,
        border: 0,
        color: "#FFF",
        height: 48,
        padding: "0 30px",
        boxShadow: "0 3px 5px 2px rgba(255, 105, 135, .3)"
      },
      containedPrimary: {
        backgroundColor: "#FF8E53"
      },
      textSecondary: {
        color: "#FFF"
      }
    },
    MuiAppBar: {
      colorPrimary: {
        background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)"
      },
      colorDefault: {
        backgroundColor: "#FF8E53"
      }
    },
    MuiTab: {
      textColorPrimary: {
        color: "#FFF"
      },
      selected: {
        color: "#FFF"
      }
    },
    MuiPaper: {
      root: {
        backgroundColor: "#FE6B8B"
      }
    },
    MuiChip: {
      colorPrimary: {
        backgroundColor: "#FE6B8B"
      }
    }
  }
});

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "100vh"
  },
  rootWithPony: {
    flexGrow: 1,
    backgroundImage: `url(${ponyBackground})`,
    backgroundSize: "cover",
    height: "100vh"
  },
  grow: {
    flexGrow: 1
  },
  content: {
    flexGrow: 1,
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2
  },
  formGroup: {
    justifyContent: "space-between",
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2
  }
});

export const AppContext = React.createContext(false);

export const wrapWithAppContext = WrappedComponent => props => (
  <AppContext.Consumer>
    {context => <WrappedComponent {...props} {...context} />}
  </AppContext.Consumer>
);

const AppLayout = ({
  children,
  classes,
  location,
  history,
  theme: defaultTheme
}) => {
  const [admin, setAdmin] = useState(false);
  const [customTheme, setCustomTheme] = useState(false);
  const isHome = Boolean(location.pathname === "/");
  return (
    <div className={customTheme ? classes.rootWithPony : classes.root}>
      <FormGroup row className={classes.formGroup}>
        <FormControlLabel
          control={
            <Switch
              checked={admin}
              onChange={event => setAdmin(event.target.checked)}
              aria-label="LoginSwitch"
            />
          }
          label={admin ? "Админ" : "Пользователь"}
        />
        <FormControlLabel
          labelPlacement={"start"}
          label={customTheme ? "Стили принцессы" : "Стили нормального человека"}
          control={
            <Switch
              checked={customTheme}
              onChange={event => setCustomTheme(event.target.checked)}
              aria-label="ThemeSwitch"
            />
          }
        />
      </FormGroup>
      <MuiThemeProvider theme={customTheme ? theme : defaultTheme}>
        <AppBar position="static">
          <Toolbar>
            {!isHome && (
              <IconButton
                color="inherit"
                aria-label="Back"
                onClick={() => history.goBack()}
              >
                <KeyboardBackspace />
              </IconButton>
            )}
            <Typography variant="h6" color="inherit" className={classes.grow}>
              Рецепты
            </Typography>
            <Button color="inherit" onClick={() => history.push("/search")}>
              Поиск рецептов
            </Button>
          </Toolbar>
        </AppBar>
        <main className={classes.content}>
          <AppContext.Provider value={{ editable: admin }}>
            {children}
          </AppContext.Provider>
        </main>
      </MuiThemeProvider>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(withRouter(AppLayout));
