import React, {useContext} from "react";
import {withRouter} from "react-router-dom";
import {AppContext} from "app/AppLayout";
import {get} from "app/request";
import {Card, CardActionArea, CardContent, CardHeader, IconButton, Typography} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import {RecipeListContext} from "./RecipeListForm";

const RecipeCard = ({classes, recipe, history}) => {
    const {editable} = useContext(AppContext);
    const {fetchRecipes} = useContext(RecipeListContext);
    const deleteRecipe = e => {
        e.stopPropagation();
        get("recipe/delete/" + recipe.id).then(() => {
            fetchRecipes();
        });
    };

    return (
        <Card
            onClick={() => {
                history.push("/recipe/" + recipe.id);
            }}
        >
            <CardActionArea>
                <CardHeader
                    action={
                        editable && (
                            <IconButton onClick={deleteRecipe}>
                                <DeleteIcon/>
                            </IconButton>
                        )
                    }
                    title={recipe.name}
                />
                <CardContent>
                    <Typography component="p">{recipe.briefDescription}</Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
};

export default withRouter(RecipeCard);
