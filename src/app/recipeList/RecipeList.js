import React from "react";
import { Grid } from "@material-ui/core";
import RecipeCard from "./RecipeCard";

const RecipeList = ({ recipes }) => {
  console.log(recipes);
  return (
    <Grid container spacing={8}>
      {recipes.map(recipe => (
        <Grid key={recipe.id} item xs={12} sm={6} md={4} lg={3}>
          <RecipeCard recipe={recipe} />
        </Grid>
      ))}
    </Grid>
  );
};

export default RecipeList;
