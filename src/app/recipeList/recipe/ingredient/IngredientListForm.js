import React, { useContext, useEffect, useState } from "react";
import { AppContext } from "app/AppLayout";
import { FieldArray } from "formik";
import {
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography,
  Button
} from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import AddIcon from "@material-ui/icons/Add";
import { AutocompleteSelectField, InputField } from "components";
import { uniqueId } from "lodash";
import IngredientForm from "./IngredientForm";
import { get } from "app/request";

const IngredientListForm = ({ values }) => {
  const { editable } = useContext(AppContext);
  const [openIngredientDialog, setOpenIngredientDialog] = useState(false);
  const [ingredientDictionary, setIngredientDictionary] = useState(false);

  useEffect(() => {
    fetchIngredientDictionary();
  }, []);

  const fetchIngredientDictionary = () => {
    get("ingredient/fetchAll").then(setIngredientDictionary);
  };

  return (
    <div style={{ overflowX: "auto", height: "100vh" }}>
      <FieldArray
        name="ingredients"
        render={arrayHelpers => (
          <Table>
            <TableHead>
              <TableRow>
                {editable && (
                  <TableCell>
                    <IconButton
                      aria-label="Add"
                      onClick={() => arrayHelpers.push({ key: uniqueId() })}
                    >
                      <AddIcon />
                    </IconButton>{" "}
                  </TableCell>
                )}
                <TableCell>Ингредиент</TableCell>
                <TableCell align="right">Количество</TableCell>
                {editable && <TableCell>Удалить</TableCell>}
              </TableRow>
            </TableHead>
            <TableBody>
              {values.ingredients &&
                values.ingredients.map((ingredient, index) => (
                  <TableRow key={ingredient.key}>
                    {editable && (
                      <TableCell>
                        <Button
                          color={"default"}
                          size={"small"}
                          onClick={() => setOpenIngredientDialog(true)}
                        >
                          Создать ингредиент
                        </Button>
                      </TableCell>
                    )}
                    <TableCell component="th" scope="row">
                      {editable ? (
                        <AutocompleteSelectField
                          options={ingredientDictionary || []}
                          name={`ingredients.${index}.ingredient`}
                          label="name"
                          placeholder="Ингредиент"
                        />
                      ) : (
                        <Typography variant="body1">
                          {values.ingredients[index].ingredient &&
                            values.ingredients[index].ingredient.name}
                        </Typography>
                      )}
                    </TableCell>
                    <TableCell align="right">
                      <InputField
                        name={`ingredients.${index}.amount`}
                        readOnly={!editable}
                      />
                    </TableCell>
                    {editable && (
                      <TableCell component="th" scope="row">
                        <IconButton
                          aria-label="Delete"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </TableCell>
                    )}
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        )}
      />
      <IngredientForm
        open={openIngredientDialog}
        onClose={() => {
          setOpenIngredientDialog(false);
          fetchIngredientDictionary();
        }}
      />
    </div>
  );
};

export default IngredientListForm;
