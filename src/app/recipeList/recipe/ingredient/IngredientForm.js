import React, { useState } from "react";
import { post } from "app/request";
import { ClosableDialogTitle } from "components";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  TextField
} from "@material-ui/core";

const IngredientForm = ({ onClose, open }) => {
  const [name, setName] = useState("");

  const saveIngredient = () => {
    console.log(name);
    console.log({ name: name });
    post("ingredient/save", { name }).then(onClose);
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <ClosableDialogTitle onClose={onClose}>Ингредиент</ClosableDialogTitle>
      <DialogContent>
        <TextField
          label="Название"
          value={name}
          onChange={event => {
            setName(event.target.value);
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button color="primary" type="button" onClick={saveIngredient}>
          Сохранить
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default IngredientForm;
