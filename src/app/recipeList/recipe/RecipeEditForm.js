import React, {useEffect, useState} from "react";
import RecipeForm from "./RecipeForm";
import { get } from "app/request";

const RecipeEditForm = ({ match }) => {
  const [recipe, setRecipe] = useState(null);

  useEffect(() => {
    const { id } = match.params;
    get("recipe/fetch/" + id).then(setRecipe);
  }, []);

  if (!recipe) {
    return null;
  }
  return <RecipeForm recipe={recipe} />;
};

export default RecipeEditForm;
