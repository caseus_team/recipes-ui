import React, { useContext, useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { AppContext } from "app/AppLayout";
import { get, post } from "app/request";
import { Form, Formik } from "formik";
import {
  AppBar,
  Button,
  Grid,
  Tab,
  Tabs,
  Toolbar,
  withStyles
} from "@material-ui/core";
import {
  AutocompleteSelectField,
  InputField,
  SimpleAlertDialog
} from "components";
import IngredientListForm from "./ingredient/IngredientListForm";
import Chip from "@material-ui/core/es/Chip/Chip";

const styles = {
  appBar: {
    top: "auto",
    bottom: 0,
    zIndex: 0
  },
  toolbar: {
    alignItems: "center",
    justifyContent: "space-between"
  }
};

const RecipeForm = ({ recipe, classes, history }) => {
  const { editable } = useContext(AppContext);
  const [openTab, setOpenTab] = useState(0);
  const [categories, setCategories] = useState([]);
  const [dialogProps, setDialogProps] = useState({ open: false });

  useEffect(() => {
    get("category/fetchAll").then(setCategories);
  }, []);

  const closeDialog = () => {
    setDialogProps({ open: false });
  };

  const editNewRecipe = recipe => {
    history.push("/recipe/" + recipe.id);
  };

  const submitForm = recipe => {
    post("recipe/save", recipe)
      .then(recipe => {
        setDialogProps({
          title: "Успех",
          content: "Рецепт успешно сохранен",
          open: true,
          onClose: () => {
            closeDialog();
            editNewRecipe(recipe);
          }
        });
      })
      .catch(error => {
        console.log(error);
        setDialogProps({
          title: "Произошла ошибка",
          content: error,
          open: true,
          onClose: closeDialog
        });
      });
  };

  const deleteRecipe = () => {
    get("recipe/delete/" + recipe.id)
      .then(() => {
        setDialogProps({
          title: "Успех",
          content: "Рецепт успешно удален",
          open: true,
          onClose: () => history.goBack()
        });
      })
      .catch(error => {
        setDialogProps({
          title: "Произошла ошибка",
          content: error,
          open: true,
          onClose: closeDialog
        });
      });
  };

  return (
    <>
      <Tabs
        value={openTab}
        onChange={(event, value) => setOpenTab(value)}
        indicatorColor="primary"
        textColor="primary"
        centered
      >
        <Tab label="Инфо" />
        <Tab label="Состав" />
        <Tab label="Шаги" />
      </Tabs>
      <Formik
        initialValues={recipe}
        onSubmit={submitForm}
        render={({ values }) => (
          <Form>
            {openTab === 0 && (
              <Grid
                container
                spacing={32}
                justify={"space-between"}
                alignItems={"center"}
              >
                <Grid item xs={12} sm={6} md={8} lg={10}>
                  <InputField
                    name="name"
                    fullWidth
                    readOnly={!editable}
                    placeholder="Введите название рецепта"
                  />
                </Grid>
                <Grid item xs={12} sm={6} md={4} lg={2}>
                  {editable ? (
                    <AutocompleteSelectField
                      name="category"
                      options={categories}
                      label="name"
                      placeholder="Выберите категорию"
                    />
                  ) : (
                    recipe &&
                    recipe.category && (
                      <Chip label={recipe.category.name} color={"primary"} />
                    )
                  )}
                </Grid>
                <Grid item xs={12}>
                  <InputField
                    name="briefDescription"
                    placeholder="Введите короткое описание рецепта"
                    multiline
                    fullWidth
                    readOnly={!editable}
                  />
                </Grid>
              </Grid>
            )}
            {openTab === 1 && <IngredientListForm values={values} />}
            {openTab === 2 && (
              <InputField
                name="description"
                label={editable ? "Описание" : ""}
                placeholder="Введите описание рецепта"
                multiline
                fullWidth
                readOnly={!editable}
              />
            )}
            {editable && (
              <AppBar
                position="fixed"
                className={classes.appBar}
                color={"default"}
              >
                <Toolbar className={classes.toolbar}>
                  <Button
                    type="button"
                    color="secondary"
                    onClick={deleteRecipe}
                  >
                    Удалить рецепт
                  </Button>
                  <Button type="submit">Сохранить</Button>
                </Toolbar>
              </AppBar>
            )}
          </Form>
        )}
      />
      <SimpleAlertDialog {...dialogProps} />
    </>
  );
};

export default withStyles(styles)(withRouter(RecipeForm));
