import React from "react";
import RecipeForm from "./RecipeForm";

const RecipeAddForm = () => <RecipeForm />;

export default RecipeAddForm;
