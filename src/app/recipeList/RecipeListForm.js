import React, { useContext, useEffect, useState } from "react";
import { get } from "app/request";
import { AppContext } from "app/AppLayout";
import AddIcon from "@material-ui/icons/Add";
import {
  withStyles,
  Button,
  Typography,
  Toolbar,
  Grid
} from "@material-ui/core";
import RecipeList from "./RecipeList";

const styles = {
  grow: {
    flexGrow: 1
  }
};

export const RecipeListContext = React.createContext({});

const RecipeListForm = ({ classes, match, history }) => {
  const { editable } = useContext(AppContext);
  const [category, setCategory] = useState(null);
  const [recipes, setRecipes] = useState(null);

  useEffect(() => {
    const categoryId = match.params.id;
    Promise.all([
      get("recipe/fetchAll/" + categoryId),
      get("category/fetch/" + categoryId)
    ]).then(([recipes, category]) => {
      setRecipes(recipes);
      setCategory(category);
    });
  }, []);

  const fetchRecipes = () => {
    const categoryId = match.params.id;
    get("recipe/fetchAll/" + categoryId).then(setRecipes);
  };

  const addRecipe = () => {
    history.push("/recipe/add");
  };
  return (
    <Grid container>
      <Grid item xs={12}>
        <Toolbar>
          {category && (
            <Typography
              variant="h5"
              color="inherit"
              noWrap
              className={classes.grow}
            >
              {category.name}
            </Typography>
          )}
          {editable && (
            <Button
              variant={"contained"}
              size={"medium"}
              color={"primary"}
              onClick={addRecipe}
            >
              <AddIcon />
              Добавить рецепт
            </Button>
          )}
        </Toolbar>
      </Grid>
      <Grid item xs={12}>
        {recipes && (
          <RecipeListContext.Provider value={{ fetchRecipes: fetchRecipes }}>
            <RecipeList recipes={recipes} onDelete={fetchRecipes} />
          </RecipeListContext.Provider>
        )}
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(RecipeListForm);
