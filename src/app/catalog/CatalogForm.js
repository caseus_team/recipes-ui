import React, { useEffect, useState } from "react";
import { withRouter } from "react-router-dom";
import { get } from "app/request";
import {
  withStyles,
  Typography,
  Toolbar,
  Grid,
  Card,
  CardContent,
  CardHeader,
  CardActionArea
} from "@material-ui/core";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    backgroundColor: theme.palette.background.paper
  },
  card: {
    maxWidth: 400
  }
});

const CatalogForm = ({ history, classes }) => {
  const [categories, setCategories] = useState([]);
  const chooseCategory = id => {
    history.push("/category/" + id);
  };
  useEffect(() => {
    get("category/fetchAll").then(setCategories);
  }, []);
  return (
    <Grid container spacing={16}>
      <Grid item xs={12} style={{ height: "auto" }}>
        <Toolbar>
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            style={{ width: "100%", textAlign: "center" }}
          >
            Выберите категорию
          </Typography>
        </Toolbar>
      </Grid>

      {categories.map(category => (
        <Grid item xs={12} sm={6} md={4} lg={3} xl={2} key={category.id}>
          <Card
            className={classes.card}
            onClick={() => chooseCategory(category.id)}
          >
            <CardActionArea>
              <CardHeader title={category.name} />
              <CardContent>
                <Typography component="p">{category.description}</Typography>
              </CardContent>
            </CardActionArea>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default withStyles(styles)(withRouter(CatalogForm));
