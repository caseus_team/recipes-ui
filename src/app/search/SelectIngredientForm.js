import React from "react";
import { FieldArray, Form, Formik } from "formik";
import Chip from "@material-ui/core/Chip";
import Grid from "@material-ui/core/Grid";
import { AutocompleteSelectField } from "components";
import Button from "@material-ui/core/Button";

const SelectIngredientForm = ({ ingredientDictionary, onSubmit }) => (
  <Formik
    initialValues={{ ingredients: [] }}
    onSubmit={onSubmit}
    render={({ values }) => (
      <Form>
        <FieldArray
          name="ingredients"
          render={arrayHelpers => (
            <Grid container spacing={16} direction={"column"}>
              <Grid item xs={12}>
                {values.ingredients &&
                  values.ingredients.map((ingredient, index) => (
                    <Chip
                      key={ingredient.id}
                      label={ingredient.name}
                      onDelete={() => arrayHelpers.remove(index)}
                    />
                  ))}
              </Grid>
              <Grid item xs={12}>
                <AutocompleteSelectField
                  name="current"
                  label="name"
                  options={ingredientDictionary}
                  placeholder="Ввыдите название"
                  onChange={value => {
                    if (
                      values.ingredients.every(
                        ingredient => ingredient.id !== value.id
                      )
                    ) {
                      arrayHelpers.push(value);
                    }
                  }}
                />
              </Grid>
              <Grid item xs>
                <Button type="submit" color="primary">
                  Поиск
                </Button>
              </Grid>
            </Grid>
          )}
        />
      </Form>
    )}
  />
);

export default SelectIngredientForm;
