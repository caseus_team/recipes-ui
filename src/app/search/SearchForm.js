import React, { useEffect, useState } from "react";
import SelectIngredientForm from "./SelectIngredientForm";
import { Grid, Typography } from "@material-ui/core";
import RecipeCard from "app/recipeList/RecipeCard";
import { get, post } from "app/request";

const SearchForm = () => {
  const [ingredientDictionary, setIngredientDictionary] = useState([]);
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    get("ingredient/fetchAll").then(setIngredientDictionary);
  }, []);

  const fetchRecipes = ingredients => {
    post("recipe/fetchAll", ingredients.ingredients).then(setRecipes);
  };

  return (
    <Grid
      container
      style={{ paddingTop: 10 }}
      spacing={16}
      direction={"column"}
    >
      <Grid item>
        <SelectIngredientForm
          ingredientDictionary={ingredientDictionary}
          onSubmit={fetchRecipes}
        />
      </Grid>
      {recipes.length > 0 ? (
        <>
          <Grid item>
            <Typography variant={"h5"}>Результаты поиска</Typography>
          </Grid>
          {recipes.map(recipe => (
            <RecipeCard recipe={recipe} />
          ))}
        </>
      ) : (
        <Typography variant={"body1"}>Нет рецептов</Typography>
      )}
    </Grid>
  );
};

export default SearchForm;
