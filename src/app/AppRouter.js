import React from "react";
import HomeForm from "./HomeForm";
import { Route, Switch } from "react-router-dom";
import RecipeListForm from "app/recipeList";
import SearchForm from "app/search";
import { RecipeEditForm, RecipeAddForm } from "app/recipeList/recipe";

const AppRouter = () => (
  <Switch>
    <Route exact path="/" component={HomeForm} />
    <Route exact path="/category/:id" component={RecipeListForm} />
    <Route exact path="/recipe/add" component={RecipeAddForm} />
    <Route exact path="/recipe/:id" component={RecipeEditForm} />
    <Route exact path="/search" component={SearchForm} />
  </Switch>
);

export default AppRouter;
