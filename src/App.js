import React from "react";
import "./App.css";
import AppLayout from "app/AppLayout";
import AppRouter from "app/AppRouter";
import { HashRouter } from "react-router-dom";

const App = () => (
  <HashRouter>
    <AppLayout>
      <AppRouter />
    </AppLayout>
  </HashRouter>
);

export default App;
